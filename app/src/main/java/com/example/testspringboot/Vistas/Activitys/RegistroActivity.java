package com.example.testspringboot.Vistas.Activitys;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.testspringboot.Presentador.GestionarJsonRegistro;
import com.example.testspringboot.R;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener {

    EditText nombre, apellidos, email, nombreUsuario, password, rol;
    Button btnRegistro;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        nombre = findViewById(R.id.nombre);
        apellidos = findViewById(R.id.apellidos);
        email = findViewById(R.id.email);
        nombreUsuario = findViewById(R.id.nombre_usuario);
        password = findViewById(R.id.password);
        rol = findViewById(R.id.rol);
        btnRegistro = findViewById(R.id.btn_registro_regitro);

        btnRegistro.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_registro_regitro:
                GestionarJsonRegistro jsonRegistro = new GestionarJsonRegistro(this, dialog);
                jsonRegistro.registrarUsuario(nombre, apellidos, email, nombreUsuario, password, rol);
                break;
        }
    }
}
