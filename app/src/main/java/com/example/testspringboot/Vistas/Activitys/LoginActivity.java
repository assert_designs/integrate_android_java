package com.example.testspringboot.Vistas.Activitys;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.testspringboot.Presentador.GestionarJsonUsuario;
import com.example.testspringboot.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText nombreUsuario;
    EditText passwordUsuario;
    Button btnIngresar, btnRegistrar, btnReset, btnNoticias, btnPublicar, btnVerificar, btnAsignar;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_main);

        nombreUsuario = findViewById(R.id.usuario);
        passwordUsuario = findViewById(R.id.contrasena);
        btnIngresar = findViewById(R.id.btn_ingresar);
        btnRegistrar = findViewById(R.id.btn_registro);
        btnReset = findViewById(R.id.btn_reset);
        btnNoticias = findViewById(R.id.btn_noticias);
        btnPublicar = findViewById(R.id.btn_publicar);
        btnVerificar = findViewById(R.id.btn_verificar);
        btnAsignar = findViewById(R.id.btn_asignar);

        btnIngresar.setOnClickListener(this);
        btnRegistrar.setOnClickListener(this);
        btnReset.setOnClickListener(this);
        btnNoticias.setOnClickListener(this);
        btnPublicar.setOnClickListener(this);
        btnVerificar.setOnClickListener(this);
        btnAsignar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_ingresar:
                GestionarJsonUsuario _jsonUsuario = new GestionarJsonUsuario(this, dialog);
                _jsonUsuario.iniciarSesion(nombreUsuario, passwordUsuario);
                break;
            case R.id.btn_registro:
                Intent intent = new Intent(this, RegistroActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_reset:
                break;
            case R.id.btn_noticias:
                break;
            case R.id.btn_publicar:
                Intent intent1 = new Intent(this, NoticiasPostActivity.class);
                startActivity(intent1);
                break;
            case R.id.btn_verificar:
                Intent intent2 = new Intent(this, CheckCt.class);
                this.startActivity(intent2);
                break;
            case R.id.btn_asignar:
                Intent intent3 = new Intent(this, AsignedCtActivity.class);
                this.startActivity(intent3);
                break;
        }
    }
}
