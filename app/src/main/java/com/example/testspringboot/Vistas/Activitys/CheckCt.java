package com.example.testspringboot.Vistas.Activitys;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.testspringboot.Presentador.GestionarJsoncheck;
import com.example.testspringboot.R;

public class CheckCt extends AppCompatActivity implements View.OnClickListener {

    EditText fecha, hora, tecnico;
    Button btnVolver, btnVerify;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_ct);

        fecha = findViewById(R.id.edit_fecha);
        hora = findViewById(R.id.edit_hora);
        tecnico = findViewById(R.id.edit_tecnico);
        btnVolver = findViewById(R.id.btn_volver);
        btnVerify = findViewById(R.id.btn_verify);

        btnVolver.setOnClickListener(this);
        btnVerify.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_volver:
                Intent intent = new Intent(this, LoginActivity.class);
                this.startActivity(intent);
                break;
            case R.id.btn_verify:
                GestionarJsoncheck jsoncheck = new GestionarJsoncheck(dialog, this);
                jsoncheck.checkCita(fecha, hora, tecnico);
                break;
        }
    }
}
