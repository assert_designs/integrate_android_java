package com.example.testspringboot.Vistas.Activitys;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.testspringboot.Presentador.GestionarJsonAsigned;
import com.example.testspringboot.R;

public class AsignedCtActivity extends AppCompatActivity implements View.OnClickListener {

    EditText fechaA, horaA, tecnicoA;
    Button btnvolverA, btnVerifyA;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asigned_ct);

        fechaA = findViewById(R.id.edit_fecha_set);
        horaA = findViewById(R.id.edit_hora_set);
        tecnicoA = findViewById(R.id.edit_tecnico_set);
        btnvolverA = findViewById(R.id.btn_volver_vol);
        btnVerifyA = findViewById(R.id.btn_asignar_asig);

        btnvolverA.setOnClickListener(this);
        btnVerifyA.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_volver_vol:
                Intent intent = new Intent(this, LoginActivity.class);
                this.startActivity(intent);
                break;
            case R.id.btn_asignar_asig:
                GestionarJsonAsigned jsonAsigned = new GestionarJsonAsigned(dialog, this);
                jsonAsigned.asignedCita(fechaA, horaA, tecnicoA);
                break;
        }
    }
}
