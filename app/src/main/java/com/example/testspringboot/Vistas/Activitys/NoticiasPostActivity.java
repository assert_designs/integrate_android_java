package com.example.testspringboot.Vistas.Activitys;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.testspringboot.Presentador.GestionarJsonNoticiasPost;
import com.example.testspringboot.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class NoticiasPostActivity extends AppCompatActivity implements View.OnClickListener {

    String fecha;
    EditText titulo, texto;
    Button btnPost;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noticias_post);

        titulo = findViewById(R.id.titulo);
        texto = findViewById(R.id.texto);
        btnPost = findViewById(R.id.btn_post);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        fecha = dateFormat.format(date);

        btnPost.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_post:
                GestionarJsonNoticiasPost jsonNoticiasPost = new GestionarJsonNoticiasPost(this, dialog);
                jsonNoticiasPost.publicar(fecha, titulo, texto);
                break;
        }
    }
}
