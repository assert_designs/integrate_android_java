package com.example.testspringboot.Modelo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.testspringboot.Entidades.Noticias;
import com.example.testspringboot.R;
import com.example.testspringboot.Vistas.Activitys.LoginActivity;
import com.example.testspringboot.Vistas.Activitys.MainActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NoticiasJsonModel {
    ProgressDialog dialog;
    Context context;

    public NoticiasJsonModel(Context context, ProgressDialog dialog){
        this.context = context;
        this.dialog = dialog;
    }

    public void postNoticias(final String fecha, final EditText texto, final EditText titulo){
        dialog = new ProgressDialog(context);
        dialog.setMessage("Cargando...");
        dialog.show();

        String url = "http://" + context.getString(R.string.ip_server) + "/noticias/post";

        Map<String, String> parametros = new HashMap<>();
        parametros.put("fecha", fecha);
        parametros.put("texto", texto.getText().toString());
        parametros.put("titulo", titulo.getText().toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(parametros), new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response){
                Noticias noticias = new Noticias();
                try{
                    noticias.setFecha(response.optString("fecha"));
                    noticias.setTexto(response.optString("texto"));
                    noticias.setTitulo(response.optString("titulo"));

                    Intent intent = new Intent(context, LoginActivity.class);
                    context.startActivity(intent);

                    Toast.makeText(context, "Publicado Correctamente", Toast.LENGTH_SHORT).show();

                    dialog.hide();
                    dialog.dismiss();
                }catch(Exception ex){
                    Toast.makeText(context, "Error al traer los datos del servidor", Toast.LENGTH_SHORT).show();
                    Log.i("ERROR_GET_DATES_NOTI", ex.toString());
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                dialog.hide();
                dialog.dismiss();

                Toast.makeText(context, "Error al validar tus datos", Toast.LENGTH_LONG).show();
                Toast.makeText(context, "Verifica e intentalo de nuevo", Toast.LENGTH_LONG).show();
                Log.i("ERROR_RESPONSE_NOTI", error.toString());
            }
        });

        PatronSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }
}
