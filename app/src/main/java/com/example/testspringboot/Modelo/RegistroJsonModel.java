package com.example.testspringboot.Modelo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.testspringboot.R;
import com.example.testspringboot.Vistas.Activitys.LoginActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegistroJsonModel {

    ProgressDialog dialog;
    Context context;

    public RegistroJsonModel(Context context, ProgressDialog dialog) {
        this.context = context;
        this.dialog = dialog;
    }

    public void registro(final EditText nombre,
                         final EditText apellidos,
                         final EditText email,
                         final EditText nombreUsuario,
                         final EditText password,
                         final EditText rol){

        dialog = new ProgressDialog(context);
        dialog.setMessage("Cargando...");
        dialog.show();

        String url = "http://" + context.getString(R.string.ip_server) + "/api/usuarios/register";

        Map<String, String> parametros = new HashMap<>();
        parametros.put("nombre", nombre.getText().toString());
        parametros.put("apellidos", apellidos.getText().toString());
        parametros.put("email", email.getText().toString());
        parametros.put("nombreusuario", nombreUsuario.getText().toString());
        parametros.put("passwordUsuario", password.getText().toString());
        parametros.put("rol", rol.getText().toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String rta = response.optString("created");
                    Log.i("RTA:", rta);
                    if(response.optString("created").equalsIgnoreCase("CREATED")){
                        Intent intent = new Intent(context, LoginActivity.class);
                        context.startActivity(intent);

                        Toast.makeText(context, "Registro Exitoso", Toast.LENGTH_SHORT).show();

                        dialog.hide();
                        dialog.dismiss();
                    }else{

                    }
                }catch (Exception ex){
                    Log.i("ERROR_RESP_REGISTER:", ex.toString());
                    Toast.makeText(context, "Error al recibir la respuesta del servidor", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.hide();
                dialog.dismiss();

                Log.i("ONERROR_RESP_REGISTER:", error.toString());
                Toast.makeText(context, "Error al registrarte verifica los datos y vuelve a intentarlo", Toast.LENGTH_LONG).show();
            }
        });

        PatronSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

    }
}
