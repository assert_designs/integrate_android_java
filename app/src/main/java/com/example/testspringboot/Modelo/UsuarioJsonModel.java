package com.example.testspringboot.Modelo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.testspringboot.Entidades.Usuario;
import com.example.testspringboot.R;
import com.example.testspringboot.Vistas.Activitys.MainActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UsuarioJsonModel {

    ProgressDialog dialog;
    Context context;

    public UsuarioJsonModel(Context context, ProgressDialog dialog){
        this.context = context;
        this.dialog = dialog;
    }

    public void iniciarSecion(final EditText nombreUsuario, final EditText passwordUsuario){
        dialog = new ProgressDialog(context);
        dialog.setMessage("Cargando...");
        dialog.show();

        String url = "http://" + context.getString(R.string.ip_server) + "/api/usuarios/login";

        Map<String, String> parametros = new HashMap<>();
        parametros.put("nombreUsuario", nombreUsuario.getText().toString());
        parametros.put("passwordUsuario", passwordUsuario.getText().toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Usuario usuario = new Usuario();
                try {
                    usuario.setId(response.optInt("id"));
                    usuario.setApellidos(response.optString("apellidos"));
                    usuario.setEmail(response.optString("email"));
                    usuario.setNombre(response.optString("nombre"));
                    usuario.setNombreUsuario(response.optString("nombreUsuario"));
                    usuario.setPasswordUsuario(response.optString("passwordUsuario"));
                    usuario.setRol(response.optString("rol"));

                    Intent intent = new Intent(context, MainActivity.class);
                    context.startActivity(intent);

                    Toast.makeText(context, "¡Bienvenido!" + usuario.getNombre() + " " + usuario.getApellidos(), Toast.LENGTH_LONG).show();

                    dialog.hide();
                    dialog.dismiss();
                } catch (Exception ex) {
                    Toast.makeText(context, ex.toString(), Toast.LENGTH_SHORT).show();
                    Log.i("ERROR_GET_DATES:", ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.hide();
                dialog.dismiss();

                Toast.makeText(context, "Usuario o Contraseña Incorrectos", Toast.LENGTH_LONG).show();
                Toast.makeText(context, "Verifica tus Datos de Nuevo", Toast.LENGTH_LONG).show();
                Log.i("ERROR_RESPONSE:", error.toString());
            }
        });

        PatronSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }
}
