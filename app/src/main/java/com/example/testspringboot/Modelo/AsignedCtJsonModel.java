package com.example.testspringboot.Modelo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.testspringboot.R;
import com.example.testspringboot.Vistas.Activitys.LoginActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AsignedCtJsonModel {

    ProgressDialog dialog;
    Context context;

    public AsignedCtJsonModel(ProgressDialog dialog, Context context) {
        this.dialog = dialog;
        this.context = context;
    }

    public void asignedCita(final EditText fecha, final EditText hora, final EditText tecnico){
        dialog = new ProgressDialog(context);
        dialog.setMessage("Cargando...");
        dialog.show();

        String url = "http://" + context.getString(R.string.ip_server) + "/citas/asignar";

        Map<String, String> parametros = new HashMap<>();
        parametros.put("fecha", fecha.getText().toString());
        parametros.put("hora", hora.getText().toString());
        parametros.put("tecnico", tecnico.getText().toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{
                    Intent intent = new Intent(context, LoginActivity.class);
                    context.startActivity(intent);

                    Toast.makeText(context, "Asignado Correctamente", Toast.LENGTH_SHORT).show();

                    dialog.hide();
                    dialog.dismiss();
                }catch(Exception ex){
                    Log.i("ERROR_DATES_ASIG:", ex.toString());
                    Toast.makeText(context, "Error in the Response", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.hide();
                dialog.dismiss();

                Log.i("ERROR_RTA_ASIG:", error.toString());
                Toast.makeText(context, "Error al recibir la respuesta del servidor", Toast.LENGTH_SHORT).show();
            }
        });

        PatronSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

    }
}
