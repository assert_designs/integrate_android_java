package com.example.testspringboot.Modelo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.testspringboot.Entidades.CheckCt;
import com.example.testspringboot.R;
import com.example.testspringboot.Vistas.Activitys.LoginActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChcekJsonModel {

    ProgressDialog dialog;
    Context context;

    public ChcekJsonModel(ProgressDialog dialog, Context context) {
        this.dialog = dialog;
        this.context = context;
    }

    public void chechCita(final EditText fecha, final EditText hora, final EditText tecnico){
        dialog = new ProgressDialog(context);
        dialog.setMessage("Cargando...");
        dialog.show();

        String url = "http://" + context.getString(R.string.ip_server) + "/citas/comprobar_cita";

        Map<String, String> parametros = new HashMap<>();
        parametros.put("fecha", fecha.getText().toString());
        parametros.put("hora", hora.getText().toString());
        parametros.put("tecnico", tecnico.getText().toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(parametros), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                CheckCt checkCt = new CheckCt();
                try{
                    if(response.optString("available").equals("AVAILABLE")){
                        checkCt.setFecha(response.optString("fecha"));
                        checkCt.setHora(response.optString("hora"));
                        checkCt.setTecnico(response.optString("tecnico"));

                        Toast.makeText(context, "Disponible", Toast.LENGTH_SHORT).show();

                        dialog.hide();
                        dialog.dismiss();
                    }else{
                        dialog.hide();
                        dialog.dismiss();
                        Toast.makeText(context, "No Disponible", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception ex){
                    dialog.hide();
                    dialog.dismiss();
                    Log.i("ERROR_RTA_CHECK:", ex.toString());
                    Toast.makeText(context, "Lo sentimos hubo un error con la respuesta del servidor", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.hide();
                dialog.dismiss();
                Log.i("ERROR_RTA_CHECK:", error.toString());
                Toast.makeText(context, "Lo sentimos hubo un error con el servidor", Toast.LENGTH_SHORT).show();
            }
        });

        PatronSingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);

    }
}
