package com.example.testspringboot.Presentador;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.EditText;

import com.example.testspringboot.Modelo.UsuarioJsonModel;

public class GestionarJsonUsuario {

    public Context context;
    public ProgressDialog dialog;

    private UsuarioJsonModel _usuarioJsonModel;

    public GestionarJsonUsuario(Context context, ProgressDialog dialog) {
        this.context = context;
        this.dialog = dialog;
        _usuarioJsonModel = new UsuarioJsonModel(context, dialog);
    }

    public void iniciarSesion(final EditText nombreUsuario, final EditText passwordUsuario){
        try{
            _usuarioJsonModel.iniciarSecion(nombreUsuario, passwordUsuario);
        }catch (Exception ex){
            throw ex;
        }
    }
}
