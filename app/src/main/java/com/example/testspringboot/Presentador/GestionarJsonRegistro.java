package com.example.testspringboot.Presentador;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.EditText;

import com.example.testspringboot.Modelo.RegistroJsonModel;

public class GestionarJsonRegistro {

    public Context context;
    public ProgressDialog dialog;

    private RegistroJsonModel registroJsonModel;

    public GestionarJsonRegistro(Context context, ProgressDialog dialog) {
        this.context = context;
        this.dialog = dialog;
        registroJsonModel = new RegistroJsonModel(context, dialog);
    }

    public void registrarUsuario(final EditText nombre,
                                 final EditText apellidos,
                                 final EditText email,
                                 final EditText nombreUsuario,
                                 final EditText password,
                                 final EditText rol){

        try{
            registroJsonModel.registro(nombre, apellidos, email, nombreUsuario, password, rol);
        }catch (Exception ex){
            throw ex;
        }

    }
}
