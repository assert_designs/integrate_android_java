package com.example.testspringboot.Presentador;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.EditText;

import com.example.testspringboot.Modelo.ChcekJsonModel;

public class GestionarJsoncheck {

    ProgressDialog dialog;
    Context context;

    private ChcekJsonModel chcekJsonModel;

    public GestionarJsoncheck(ProgressDialog dialog, Context context) {
        this.dialog = dialog;
        this.context = context;
        chcekJsonModel = new ChcekJsonModel(dialog, context);
    }

    public void checkCita(final EditText fecha, final EditText hora, final EditText tecnico){
        try{
            chcekJsonModel.chechCita(fecha, hora, tecnico);
        }catch(Exception ex){
            throw ex;
        }
    }
}
