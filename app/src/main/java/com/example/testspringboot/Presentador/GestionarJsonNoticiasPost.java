package com.example.testspringboot.Presentador;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.EditText;

import com.example.testspringboot.Modelo.NoticiasJsonModel;

public class GestionarJsonNoticiasPost {

    public Context context;
    public ProgressDialog dialog;

    private NoticiasJsonModel noticiasJsonModel;

    public GestionarJsonNoticiasPost(Context context, ProgressDialog dialog){
        this.context = context;
        this.dialog = dialog;
        noticiasJsonModel = new NoticiasJsonModel(context, dialog);
    }

    public void publicar(final String fecha, final EditText titulo, final EditText texto){
        try{
            noticiasJsonModel.postNoticias(fecha, titulo, texto);
        }catch(Exception ex){
            throw ex;
        }
    }
}
