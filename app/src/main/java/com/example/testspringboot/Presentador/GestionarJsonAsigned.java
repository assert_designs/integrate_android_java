package com.example.testspringboot.Presentador;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.EditText;

import com.example.testspringboot.Modelo.AsignedCtJsonModel;

public class GestionarJsonAsigned {

    ProgressDialog dialog;
    Context context;

    private AsignedCtJsonModel asignedCtJsonModel;

    public GestionarJsonAsigned(ProgressDialog dialog, Context context) {
        this.dialog = dialog;
        this.context = context;
        asignedCtJsonModel = new AsignedCtJsonModel(dialog, context);
    }

    public void asignedCita(final EditText fecha, final EditText hora, final EditText tecnico){
        try{
            asignedCtJsonModel.asignedCita(fecha, hora, tecnico);
        }catch (Exception ex){
            throw ex;
        }
    }
}
